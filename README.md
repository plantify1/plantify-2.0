# Back-end pour l'application Plantify

Dans le cadre d'une mise en situation professionnelle, nous avons du réaliser une application permettant de gérer le gardiennage de plantes.


## Authors

- Emmanuel Crenn
- Damasceno Remy
- Derriennic Quentin
- Vernier-Lambert Antoine


## Docker

Création du conteneur docker contenant la base de données MySQL

```bash
  docker run --name plantify -p 3306:3306 -e MYSQL_USER=java -e MYSQL_PASSWORD=java -e MYSQL_DATABASE=plantify -e MYSQL_ROOT_PASSWORD=root -d mysql
```

## OpenAPI

```html
    http://localhost:8081/swagger-ui/index.html
```

## API Reference

### user-controller

#### Get all users

```http
  GET /plantify/users/
```
response
```js
[
  {
    "id": 0,
    "uidFirebase": "string",
    "login": "string",
    "address": "string"
  }
]
```

#### Create user

```http
  POST /plantify/users/
```
Request body
```js
{
  "uidFirebase": "string",
  "login": "string",
  "address": "string"
}
```

#### Get a user by id with roles

```http
  GET /plantify/users/{id}
```
response
```js
{
  "id": 0,
  "uidFirebase": "string",
  "login": "string",
  "address": "string",
  "roles": [
    {
      "id": 0,
      "name": "ROLE_USER"
    }
  ]
}
```


### plant-controller

#### Create plant with ownerId

```http
  POST /plantify/plant/{ownerId}
```
Request body
```js
{
  "name": "string",
  "toGuard": true
}
```

#### Get plant by id

```http
  GET /plantify/plant/{id}
```
Response
```js
{
  "name": "string",
  "toGuard": true,
  "owner": {
    "id": 0,
    "uidFirebase": "string",
    "login": "string",
    "address": "string"
  },
  "guard": {
    "id": 0,
    "uidFirebase": "string",
    "login": "string",
    "address": "string"
  }
}
```

#### Get all plant

```http
  GET /plantify/plant/
```
Response
```js
[
  {
    "name": "string",
    "toGuard": true,
    "owner": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    },
    "guard": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    }
  }
]
```

#### Modify a plant

```http
  PUT /plantify/plant/{plantId}
```
Request body
```js
{
  "name": "string",
  "toGuard": true
}
```

#### message-controller

#### Create message

```http
  POST /plantify/message/
```
Request body
```js
{
  "messageBody": "string",
  "messageObject": "string",
  "senderUidFirebase": "string",
  "loginReceiver": "string"
}
```

#### Get all messages received by a user

```http
  GET /plantify/message/{receiverId}
```
Response
```js
[
  {
    "messageBody": "string",
    "messageObject": "string",
    "messageDate": "2023-07-08T20:25:51.674Z",
    "sender": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    },
    "receiver": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    }
  }
]
```

#### Get all messages sent by a user

```http
  GET /plantify/message/send/{senderId}
```
Response
```js
[
  {
    "messageBody": "string",
    "messageObject": "string",
    "messageDate": "2023-07-08T20:25:51.674Z",
    "sender": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    },
    "receiver": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    }
  }
]
```

### user-comment-plant-controller

#### Create a comment

```http
  POST /plantify/comment/
```
Request body
```js
{
  "comment": "string",
  "botanistId": 0,
  "plantId": 0
}
```

#### Get comment for a plant

```http
  GET /plantify/comment/{id}
```
Response
```js
[
  {
    "comment": "string",
    "botanist": {
      "id": 0,
      "uidFirebase": "string",
      "login": "string",
      "address": "string"
    },
    "date": "2023-07-09T13:04:15.699Z"
  }
]
```

