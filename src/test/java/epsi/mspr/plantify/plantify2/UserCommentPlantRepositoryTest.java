package epsi.mspr.plantify.plantify2;

import epsi.mspr.plantify.plantify2.model.Plant;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.model.UserCommentPlant;
import epsi.mspr.plantify.plantify2.repository.PlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserCommentPlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class UserCommentPlantRepositoryTest {

    @Autowired
    private UserCommentPlantRepository userCommentPlantRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PlantRepository plantRepository;

    @Test
    void testCreateComment(){
        //ETANT donné un botaniste
        User botanist = userRepository.findById(4L).orElseThrow();

        //ETANT donné une plante
        Plant plant = plantRepository.findById(1L).orElseThrow();

        //ETANT donné un commentaire
        UserCommentPlant userCommentPlant = new UserCommentPlant();
        userCommentPlant.setComment("C'est un commentaire");
        userCommentPlant.setPlant(plant);
        userCommentPlant.setBotanist(botanist);

        //QUAND je sauvegarde le commentaire
        UserCommentPlant userCommentPlantDeux = userCommentPlantRepository.save(userCommentPlant);

        //ALORS le commentaire est sauvegardé
        Assertions.assertNotNull(userCommentPlantDeux);
        Assertions.assertEquals("C'est un commentaire", userCommentPlantDeux.getComment());
    }

    @Test
    void testGetCommentByPlantId(){
        //ETANT donné un id de plante
        long id = 1L;

        //ETANT donné la liste des commentaires de cette plante
        List<UserCommentPlant> userCommentPlantList = userCommentPlantRepository.findByPlantId(id);

        //ALORS la liste n'est pas vide
        Assertions.assertFalse(userCommentPlantList.isEmpty());

        //ALORS la liste contient bien le commentaire que j'attendais
        Assertions.assertEquals("Arroser 3x par jour", userCommentPlantList.get(0).getComment());
    }


}
