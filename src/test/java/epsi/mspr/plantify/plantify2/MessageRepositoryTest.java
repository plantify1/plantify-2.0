package epsi.mspr.plantify.plantify2;

import epsi.mspr.plantify.plantify2.model.Message;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.MessageRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class MessageRepositoryTest {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    void testCreateMessage() {
        //ETANT donné un emetteur
        User emmeteur = userRepository.findById(4L).orElseThrow();

        //ETANT donné un destinataire
        User destinataire = userRepository.findById(5L).orElseThrow();

        //ETANT donné un message
        Message message = new Message();
        message.setSender(emmeteur);
        message.setReceiver(destinataire);
        message.setMessageBody("Bonjour");
        message.setMessageObject("Bonjour");

        //QUAND je sauvegarde le message
        Message messageDeux = messageRepository.save(message);

        //ALORS le message est sauvegardé et les champs sont bien remplis
        Assertions.assertNotNull(messageDeux);
        Assertions.assertEquals("Bonjour", messageDeux.getMessageBody());
        Assertions.assertEquals("Bonjour", messageDeux.getMessageObject());
        Assertions.assertEquals("camille.dubois", messageDeux.getSender().getLogin());
        Assertions.assertEquals("luc.bernard", messageDeux.getReceiver().getLogin());
    }

    @Test
    void testGetallMessagesReceivedByUser(){
        //ETANT donné un utilisateur
        User user = userRepository.findById(1L).orElseThrow();

        //QUAND je récupère tous les messages reçus par cet utilisateur
        List<Message> messages = messageRepository.findAllByReceiverIdOrderByMessageDateDesc(user.getId()).orElseThrow();

        //ALORS j'ai bien tous les messages
        Assertions.assertNotNull(messages);
        Assertions.assertEquals(2, messages.size());

        //ETANT donné un message de cette liste
        Message message = messages.get(0);

        //ALORS le message est bien celui que j'attendais
        Assertions.assertEquals("Coucou", message.getMessageBody());
    }

    @Test
    void testGetallMessagesSendByUser(){
        //ETANT donné un utilisateur
        User user = userRepository.findById(1L).orElseThrow();

        //QUAND je récupère tous les messages reçus par cet utilisateur
        List<Message> messages = messageRepository.findAllBySenderIdOrderByMessageDateDesc(user.getId()).orElseThrow();

        //ALORS j'ai bien tous les messages
        Assertions.assertNotNull(messages);
        Assertions.assertEquals(2, messages.size());

        //ETANT donné un message de cette liste
        Message message = messages.get(0);

        //ALORS le message est bien celui que j'attendais
        Assertions.assertEquals("Bonjour", message.getMessageBody());
    }

}
