package epsi.mspr.plantify.plantify2;

import epsi.mspr.plantify.plantify2.model.Plant;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.PlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class PlantRepositoryTest {
    @Autowired
    private PlantRepository plantRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    void testCreatePlantWithOwnerId() {
        //Ettant donné un id d'utilisateur
        Long id = 4L;

        //ET ETANT donné un utilisateur
        User user = userRepository.findById(id).orElseThrow();

        //ET ETANT donné une plante avec cet utilisateur comme propriétaire
        Plant plant = new Plant();
        plant.setName("tulipe");
        plant.setOwner(user);

        //QUAND je sauvegarde la plante
        Plant plantDeux = plantRepository.save(plant);

        //ALORS la plante est sauvegardée
        Assertions.assertNotNull(plantDeux);
        Assertions.assertEquals("tulipe", plantDeux.getName());
        Assertions.assertEquals("camille.dubois", plantDeux.getOwner().getLogin());
    }

    @Test
    void testGetPlantById() {
        //ETANT donné un id de plante
        Long id = 1L;

        //ETANT donné une plante avec cet id
        Plant plant = plantRepository.findById(id).orElseThrow();

        //ALORS la plante est bien celle que j'attendais
        Assertions.assertEquals("Rose", plant.getName());
        Assertions.assertEquals("jean.dupont", plant.getOwner().getLogin());
    }

    @Test
    void testGetAllPlant() {
        //Quand je récupère toutes les plantes
        Iterable<Plant> plants = plantRepository.findAll();

        //Alors j'ai bien toutes les plantes
        Assertions.assertNotNull(plants);
        Assertions.assertEquals(5, plants.spliterator().getExactSizeIfKnown());

        //ETANT donné une plante de cette liste
        Plant plant = plants.iterator().next();

        //ALORS la plante est bien celle que j'attendais
        Assertions.assertEquals("Rose", plant.getName());
        Assertions.assertEquals("jean.dupont", plant.getOwner().getLogin());
    }

    @Test
    void testModifyPlant(){
        //ETANT donné un id de plante
        Long id = 1L;

        //ETANT donné une plante avec cet id
        Plant plant = plantRepository.findById(id).orElseThrow();

        //QUAND je modifie la plante
        plant.setToGuard(true);
        plantRepository.save(plant);

        //ALORS la plante est bien modifiée
        Assertions.assertTrue(plant.isToGuard());
    }
}
