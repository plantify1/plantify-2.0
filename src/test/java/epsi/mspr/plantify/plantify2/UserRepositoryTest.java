package epsi.mspr.plantify.plantify2;

import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE  )
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void testCreateUser() {
        //ETANT donné un utilisateur
        User user = new User();
        user.setLogin("John Doe");
        user.setUidFirebase("0000");
        user.setAddress("1 rue du calvaire");

        //QUAND je sauvegarde l'utilisateur
        User userDeux = userRepository.save(user);

        //ALORS l'utilisateur est sauvegardé
        Assertions.assertNotNull(userDeux);
        Assertions.assertEquals("John Doe", userDeux.getLogin());
        Assertions.assertEquals("0000", userDeux.getUidFirebase());
        Assertions.assertEquals("1 rue du calvaire", userDeux.getAddress());
    }

    @Test
    void testGetAllUser() {
        //Quand je récupère tous les utilisateurs
        List<User> users = userRepository.findAll();

        //Alors j'ai bien tous les utilisateurs
        Assertions.assertNotNull(users);
        Assertions.assertEquals(5, users.size());

        //ETANT donné un utilisateur de cette liste
        User user = users.get(0);

        //ALORS l'utilisateur est bien celui que j'attendais
        Assertions.assertEquals("jean.dupont", user.getLogin());
        Assertions.assertEquals("gqdgqdfFfxv", user.getUidFirebase());
        Assertions.assertEquals("123 Rue Principale", user.getAddress());
    }

    @Test
    void testGetUserByUidFirebase() {
        //ETANT donné un uidFirebase
        String uidFirebase = "qgfqsdgfq54q74g5qdf4g";

        //QUAND je récupère l'utilisateur par son uidFirebase
        User user = userRepository.findByUidFirebase(uidFirebase);

        //ALORS l'utilisateur est bien celui que j'attendais
        Assertions.assertNotNull(user);
        Assertions.assertEquals("pierre.martin", user.getLogin());
        Assertions.assertEquals("789 Chemin du Soleil", user.getAddress());
    }

    @Test
    void testGetUserByIdWithRole(){
        //ETANT donné un id d'utilisateur
        Long id = 4L;

        //QUAND je récupère l'utilisateur par son id
        User user = userRepository.findById(id).orElseThrow();

        //ALORS l'utilisateur a bien un role
        Assertions.assertNotNull(user.getRoles());

        //Avec les bons roles
        Assertions.assertEquals("ROLE_BOTANIST", user.getRoles().get(0).getName().name());
    }
}
