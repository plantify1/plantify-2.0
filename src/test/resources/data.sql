USE `plantifytest`;

-- Insertion de données dans la table `user`
INSERT INTO `user` (id, `address`, `login`, `uid_firebase`)
VALUES
    (1,'123 Rue Principale', 'jean.dupont', 'gqdgqdfFfxv'),
    (2,'456 Avenue des Fleurs', 'marie.leroy', 'ern54654qgqfdgq'),
    (3,'789 Chemin du Soleil', 'pierre.martin', 'qgfqsdgfq54q74g5qdf4g'),
    (4,'321 Boulevard des Roses', 'camille.dubois', 'q5f4g5dqf4g5dqfg4qd54g'),
    (5,'654 Rue des Lilas', 'luc.bernard', '5f46g4dfq54g5qdfg5qd');

-- Insertion de données dans la table `message`
INSERT INTO `message` (id, `message_body`, `message_date`, `receiver_id`, `sender_id`, `message_object`)
VALUES
    (1, 'Bonjour', NOW(), 2, 1, 'Salutations'),
    (2, 'Coucou', NOW(), 1, 2, 'Message amical'),
    (3, 'Comment ça va ?', NOW(), 3, 1, 'Demande de nouvelles'),
    (4, 'Enchanté de vous rencontrer', NOW(), 2, 3, 'Présentation'),
    (5, 'Bonne journée !', NOW(), 1, 3, 'Vœu de bonne journée');

-- Insertion de données dans la table `plant`
INSERT INTO `plant` (id, `name`, `to_guard`, `user_id`)
VALUES
    (1, 'Rose', 1, 1),
    (2, 'Tulipe', 0, 2),
    (3, 'Tournesol', 1, 3),
    (4,'Lys', 1, 4),
    (5, 'Orchidée', 0, 5);

-- Insertion de données dans la table `picture`
INSERT INTO `picture` (id, `picture`, `plant_id`)
VALUES
    (1, 'rose.jpg', 1),
    (2, 'tulipe.jpg', 2),
    (3, 'tournesol.jpg', 3),
    (4, 'lys.jpg', 4),
    (5, 'orchidee.jpg', 5);

-- Insertion de données dans la table `role`
INSERT INTO `role` (id, `name`)
VALUES
    (1, 0),
    (2, 1),
    (3, 2);

-- Insertion de données dans la table `user-has-role`
INSERT INTO `user_has_role` (`role_id`, `user_id`)
VALUES
    (1, 1),
    (2, 2),
    (2, 3),
    (3, 4),
    (3, 5),
    (3, 1),
    (3, 2),
    (3, 3);

-- Insertion de données dans la table `user_comment_plant`
INSERT INTO `user_comment_plant` (id, `comment`, `date`, `botanist_id`, `plant_id`)
VALUES
    (1, 'Arroser 3x par jour', NOW(), 1, 1),
    (2, 'Cueillir le matin', NOW(), 2, 2),
    (3, 'Faire attention aux limaces', NOW(), 3, 3),
    (4, 'Le soleil est son ami', NOW(), 4, 4),
    (5, 'Arrosé le soir de pleine lune', NOW(), 5, 5);

-- Insertion de données dans la table `user_guard_plant`
INSERT INTO `user_guard_plant` (id, `date`, `guard_id`, `plant_id`)
VALUES
    (1, NOW(), 1, 1),
    (2, NOW(), 2, 2),
    (3, NOW(), 3, 3),
    (4, NOW(), 4, 4),
    (5, NOW(), 5, 5);