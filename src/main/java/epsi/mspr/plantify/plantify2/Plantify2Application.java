package epsi.mspr.plantify.plantify2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Plantify2Application {

    public static void main(String[] args) {
        SpringApplication.run(Plantify2Application.class, args);
    }

}
