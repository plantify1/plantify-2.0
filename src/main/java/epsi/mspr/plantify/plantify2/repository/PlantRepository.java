package epsi.mspr.plantify.plantify2.repository;

import epsi.mspr.plantify.plantify2.model.Plant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * PlantRepository   is the interface to communicate with the plant table in the database
 */
@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

}
