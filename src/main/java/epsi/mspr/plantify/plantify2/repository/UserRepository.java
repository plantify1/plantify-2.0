package epsi.mspr.plantify.plantify2.repository;

import epsi.mspr.plantify.plantify2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * UserRepository   is the interface to communicate with the user table in the database
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByLogin(String login);
    @Modifying
    @Query(value = "insert into user_has_role (role_id, user_id) values (:roleId, :userId)", nativeQuery = true)
    void saveRole(Long userId, Long roleId);

    @Query(value = "select * from user where id = (select user_id from user_guard_plant u where plant_id = :plantId order by u.date Desc Limit 1)", nativeQuery = true)
    User findGuardByPlantId(Long plantId);

    User findByUidFirebase(String s);

    User getUserByUidFirebase(String uidFirebase);

    User getUserByLogin(String login);
}
