package epsi.mspr.plantify.plantify2.repository;

import epsi.mspr.plantify.plantify2.model.UserCommentPlant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * UserCommentPlantRepository   is the interface to communicate with the user_comment_plant table in the database
 */
public interface UserCommentPlantRepository extends JpaRepository<UserCommentPlant, Long> {
    List<UserCommentPlant> findByPlantId(long plantId);
}
