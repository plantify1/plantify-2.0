package epsi.mspr.plantify.plantify2.repository;

import epsi.mspr.plantify.plantify2.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * MessageRepository   is the interface to communicate with the message table in the database
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    Optional <List<Message>> findAllByReceiverIdOrderByMessageDateDesc(long id);
    Optional <List<Message>> findAllBySenderIdOrderByMessageDateDesc(long id);

}
