package epsi.mspr.plantify.plantify2.api.controller;

import epsi.mspr.plantify.plantify2.api.dto.CommentDTO;
import epsi.mspr.plantify.plantify2.api.dto.CommentResponseDTO;
import epsi.mspr.plantify.plantify2.service.UserCommentPlantService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller for Advice
 */

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping(path="/plantify/comment", produces = APPLICATION_JSON_VALUE)
public class UserCommentPlantController {

    private final UserCommentPlantService userCommentPlantService;
    @Operation(summary = "Create a comment")
    @PostMapping("/")
    public ResponseEntity<?> createComment(@RequestBody CommentDTO commentDTO) {
        userCommentPlantService.saveComment(commentDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Get comment for a plant")
    @GetMapping("/{id}")
    public ResponseEntity<List<CommentResponseDTO>> getComment(@PathVariable("id") long plantId) {
        List<CommentResponseDTO> commentResponseDTOS = userCommentPlantService.getComment(plantId);
        return new ResponseEntity<>(commentResponseDTOS, HttpStatus.OK);
    }




}
