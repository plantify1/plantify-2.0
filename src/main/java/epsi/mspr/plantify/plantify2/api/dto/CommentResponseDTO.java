package epsi.mspr.plantify.plantify2.api.dto;

import epsi.mspr.plantify.plantify2.model.User;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.Instant;

/**
 * CommentResponseDTO   is the class that represents the comment response data transfer object
 */

@Data
public class CommentResponseDTO {
    @NotBlank
    private String comment;
    @NotBlank
    private User botanist;
    @NotBlank
    private Instant date;

}
