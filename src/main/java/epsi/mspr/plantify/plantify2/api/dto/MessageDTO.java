package epsi.mspr.plantify.plantify2.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * MessageDTO   is the class that represents the message data transfer object
 */
@Data
@AllArgsConstructor
public class MessageDTO {
    @NotBlank
    private String messageBody;
    @NotBlank
    private String messageObject;
    @NotBlank
    private String senderUidFirebase;
    @NotBlank
    private String loginReceiver;
}
