package epsi.mspr.plantify.plantify2.api.dto;

import epsi.mspr.plantify.plantify2.model.Role;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * UserWithRoleDTO   is the class that represents the user with role data transfer object
 */
@Getter
@Setter
public class UserWithRoleDTO {
    private Long id;
    @NotBlank
    private String uidFirebase;
    @NotBlank
    private String login;
    private String address;
    @NotBlank
    private List<Role> roles;
}
