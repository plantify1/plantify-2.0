package epsi.mspr.plantify.plantify2.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * PlantDTO   is the class that represents the plant data transfer object
 */
@Data
public class PlantDTO {
    @NotBlank
    private String name;
    private boolean toGuard;
}
