package epsi.mspr.plantify.plantify2.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

/**
 * MessageResponseDTO   is the class that represents the message response data transfer object
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponseDTO {
    @NotBlank
    private String messageBody;
    @NotBlank
    private String messageObject;
    @NotBlank
    private Instant messageDate;
    @NotBlank
    private UserDTO sender;
    @NotBlank
    private UserDTO receiver;
}
