package epsi.mspr.plantify.plantify2.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * PlantResponseDTO   is the class that represents the plant response data transfer object
 */
@Data
public class PlantResponseDTO {
    @NotBlank
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private boolean toGuard;
    @NotBlank
    private UserDTO owner;
    private UserDTO guard;

}
