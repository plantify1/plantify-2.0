package epsi.mspr.plantify.plantify2.api.dto;

import epsi.mspr.plantify.plantify2.model.User;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

/**
 * UserDTO   is the class that represents the user data transfer object
 */
@Getter
@Setter
public class UserDTO {
    private Long id;
    @NotBlank
    private String uidFirebase;
    @NotBlank
    private String login;
    private String address;

    public static UserDTO userToDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setUidFirebase(user.getUidFirebase());
        userDTO.setAddress(user.getAddress());
        return userDTO;
    }
}
