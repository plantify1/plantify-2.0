package epsi.mspr.plantify.plantify2.api.controller;

import epsi.mspr.plantify.plantify2.api.dto.PlantDTO;
import epsi.mspr.plantify.plantify2.api.dto.PlantResponseDTO;
import epsi.mspr.plantify.plantify2.service.PlantService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller for plants
 */

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping(path="/plantify/plant", produces = APPLICATION_JSON_VALUE)
public class PlantController {

    private final PlantService plantService;

    @Operation(summary = "Create a plant")
    @PostMapping("/{ownerId}")
    public ResponseEntity<?> createPlant(@RequestBody PlantDTO plantDTO, @PathVariable(name = "ownerId") Long ownerId) {
        plantService.savePlant(plantDTO, ownerId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Get a plant")
    @GetMapping("/{id}")
    public ResponseEntity<PlantResponseDTO> getPlant(@PathVariable(name = "id") Long id){
        PlantResponseDTO plant = plantService.getPlant(id);
        return new ResponseEntity<>(plant, HttpStatus.OK);
    }

    @Operation(summary = "Get all plants")
    @GetMapping("/")
    public ResponseEntity<List<PlantResponseDTO>> getMessagesReceived(){
        List<PlantResponseDTO> plants = plantService.getAllPlants();
        return new ResponseEntity<>(plants, HttpStatus.OK);
    }

    @Operation(summary = "modify a plant")
    @PutMapping("/{plantId}")
    public ResponseEntity<?> modifyPlant(@RequestBody PlantDTO plantDTO, @PathVariable(name = "plantId") Long plantId) {
        plantService.modifyPlant(plantDTO, plantId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
