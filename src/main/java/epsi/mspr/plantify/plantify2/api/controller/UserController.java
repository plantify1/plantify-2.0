package epsi.mspr.plantify.plantify2.api.controller;

import epsi.mspr.plantify.plantify2.api.dto.UserDTO;
import epsi.mspr.plantify.plantify2.api.dto.UserWithRoleDTO;
import epsi.mspr.plantify.plantify2.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller for users
 */

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping(path="/plantify/users", produces = APPLICATION_JSON_VALUE)
public class UserController {
    private final UserService userService;

    @Operation(summary = "Create a user")
    @PostMapping("/")
    public ResponseEntity<?> createUser(@RequestBody UserDTO user) {
        if(!userService.createUser(user))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Get all users")
    @GetMapping("/")
    public ResponseEntity<List<UserDTO>> getAllUsers(){
        try {
            List<UserDTO> users = userService.getAllUsers();
            return new ResponseEntity<>(users, HttpStatus.ACCEPTED);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "Get a user by id with roles")
    @GetMapping("/{id}")
    public ResponseEntity<UserWithRoleDTO> getUserByIdWithRole(@PathVariable("id") long id) {
        UserWithRoleDTO user = userService.getUserWithRole(id);
        if(user == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
