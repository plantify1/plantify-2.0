package epsi.mspr.plantify.plantify2.api.controller;

import epsi.mspr.plantify.plantify2.api.dto.MessageDTO;
import epsi.mspr.plantify.plantify2.api.dto.MessageResponseDTO;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.service.MessageService;
import epsi.mspr.plantify.plantify2.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller for messages
 */

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping(path="/plantify/message", produces = APPLICATION_JSON_VALUE)
public class MessageController {
    private final MessageService messageService;
    @Operation(summary = "Create a message")
    @PostMapping("/")
    public ResponseEntity<?> createMessage(@RequestBody MessageDTO messageDTO) {
        try{
            messageService.saveMessage(messageDTO);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "Get all messages received by a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the messages")})
    @GetMapping("/{receiverId}")
    public ResponseEntity<List<MessageResponseDTO>> getMessagesReceived(@PathVariable(name = "receiverId") String receiverUid){
        List<MessageResponseDTO> messages = messageService.getMessagesReceived(receiverUid);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @Operation(summary = "Get all messages sent by a user")
    @GetMapping("/send/{senderId}")
    public ResponseEntity<List<MessageResponseDTO>> getMessagesSend(@PathVariable(name = "senderId") String senderUid){
        List<MessageResponseDTO> messages = messageService.getMessagesSend(senderUid);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }
}
