package epsi.mspr.plantify.plantify2.api.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * CommentDTO   is the class that represents the comment data transfer object
 */
@Getter
@AllArgsConstructor
public class CommentDTO {
    @NotBlank
    private String comment;
    @NotBlank
    private Long botanistId;
    @NotBlank
    private Long plantId;
}
