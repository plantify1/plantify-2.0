package epsi.mspr.plantify.plantify2.service.impl;

import epsi.mspr.plantify.plantify2.api.dto.PlantDTO;
import epsi.mspr.plantify.plantify2.api.dto.PlantResponseDTO;
import epsi.mspr.plantify.plantify2.api.dto.UserDTO;
import epsi.mspr.plantify.plantify2.model.Plant;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.PlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import epsi.mspr.plantify.plantify2.service.PlantService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * PlantServiceImpl   is the class that implements the PlantService interface
 */
@Service
@RequiredArgsConstructor
public class PlantServiceImpl implements PlantService {
    private final PlantRepository plantRepository;
    private final UserRepository userRepository;
    @Override
    @Transactional
    public void savePlant(PlantDTO plantDTO, Long ownerId) {
        //create a plant with the DTO
        Plant plant = new Plant();
        plant.setName(plantDTO.getName());
        plant.setToGuard(plantDTO.isToGuard());
        //verify if the user exists
        User owner = userRepository.findById(ownerId).orElseThrow();
        //set the owner of the plant
        plant.setOwner(owner);
        //save the plant
        plantRepository.save(plant);
    }

    @Override
    @Transactional
    public PlantResponseDTO getPlant(Long plantId) {
        //get the plant
        Plant plant = plantRepository.findById(plantId).orElseThrow();
        return plantToDto(plant);
    }

    @Override
    @Transactional
    public List<PlantResponseDTO> getAllPlants() {
        //get all the plants
        List<Plant> plants = plantRepository.findAll();
        List<PlantResponseDTO> plantResponseDTOS = new ArrayList<>();
        for(Plant plant : plants){
            plantResponseDTOS.add(plantToDto(plant));
        }
        return plantResponseDTOS;
    }

    @Override
    @Transactional
    public void modifyPlant(PlantDTO plantDTO, Long plantId) {
        //modify the plant
        Plant plant = plantRepository.findById(plantId).orElseThrow();
        plant.setName(plantDTO.getName());
        plant.setToGuard(plantDTO.isToGuard());
        plantRepository.save(plant);
    }

    private PlantResponseDTO plantToDto(Plant plant) {
        //create a DTO with the plant
        PlantResponseDTO plantResponseDTO = new PlantResponseDTO();
        plantResponseDTO.setId(plant.getId());
        plantResponseDTO.setName(plant.getName());
        plantResponseDTO.setToGuard(plant.isToGuard());
        plantResponseDTO.setOwner(UserDTO.userToDto(userRepository.findById(plant.getOwner().getId()).orElseThrow()));
        //if the plant is to guard, set the guard
        if (plant.isToGuard()) {
            plantResponseDTO.setGuard(UserDTO.userToDto(userRepository.findGuardByPlantId(plant.getId())));
        }
        return plantResponseDTO;
    }
}
