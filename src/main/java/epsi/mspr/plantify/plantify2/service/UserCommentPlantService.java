package epsi.mspr.plantify.plantify2.service;

import epsi.mspr.plantify.plantify2.api.dto.CommentDTO;
import epsi.mspr.plantify.plantify2.api.dto.CommentResponseDTO;

import java.util.List;

public interface UserCommentPlantService {
    void saveComment(CommentDTO commentDTO);
    List<CommentResponseDTO> getComment(long plantId);
}
