package epsi.mspr.plantify.plantify2.service.impl;

import epsi.mspr.plantify.plantify2.api.dto.MessageDTO;
import epsi.mspr.plantify.plantify2.api.dto.MessageResponseDTO;
import epsi.mspr.plantify.plantify2.api.dto.UserDTO;
import epsi.mspr.plantify.plantify2.exception.UserNotFoundException;
import epsi.mspr.plantify.plantify2.model.Message;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.MessageRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import epsi.mspr.plantify.plantify2.service.MessageService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * MessageServiceImpl   is the class that implements the MessageService interface
 */
@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;

    private final UserRepository userRepository;
    @Override
    @Transactional
    public void saveMessage(MessageDTO messageDTO) {
        User sender = userRepository.getUserByUidFirebase(messageDTO.getSenderUidFirebase());
        User receiver = userRepository.getUserByLogin(messageDTO.getLoginReceiver());
        if(sender == null || receiver == null){
            throw new UserNotFoundException("User not found");
        }
        //get the current date
        Instant date = Instant.now();
        //create the message
        Message message = new Message();
        message.setMessageBody(messageDTO.getMessageBody());
        message.setMessageObject(messageDTO.getMessageObject());
        message.setMessageDate(date);
        message.setSender(sender);
        message.setReceiver(receiver);
        //save the message
        messageRepository.save(message);
    }

    @Override
    @Transactional
    public List<MessageResponseDTO> getMessagesSend(String uidFirebase) {
        //get all the messages send by the user
        User user = userRepository.findByUidFirebase(uidFirebase);
        List<Message> messages = messageRepository.findAllBySenderIdOrderByMessageDateDesc(user.getId()).orElse(new ArrayList<>());
        return getMessageResponseDTOS(messages);
    }

    @Override
    @Transactional
    public List<MessageResponseDTO> getMessagesReceived(String uidFirebase) {
        //get all the messages received by the user
        User user = userRepository.findByUidFirebase(uidFirebase);
        List<Message> messages = messageRepository.findAllByReceiverIdOrderByMessageDateDesc(user.getId()).orElse(new ArrayList<>());
        return getMessageResponseDTOS(messages);
    }

    private List<MessageResponseDTO> getMessageResponseDTOS(List<Message> messages) {
        //convert the messages to MessageResponseDTO
        List<MessageResponseDTO> messageResponseDTOS = new ArrayList<>();
        for(Message message : messages){
            MessageResponseDTO messageResponseDTO = new MessageResponseDTO();
            messageResponseDTO.setMessageBody(message.getMessageBody());
            messageResponseDTO.setMessageObject(message.getMessageObject());
            messageResponseDTO.setMessageDate(message.getMessageDate());
            messageResponseDTO.setReceiver(UserDTO.userToDto(message.getReceiver()));
            messageResponseDTO.setSender(UserDTO.userToDto(message.getSender()));
            messageResponseDTOS.add(messageResponseDTO);
        }
        return messageResponseDTOS;
    }
}
