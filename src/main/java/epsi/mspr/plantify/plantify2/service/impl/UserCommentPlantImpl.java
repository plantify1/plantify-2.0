package epsi.mspr.plantify.plantify2.service.impl;

import epsi.mspr.plantify.plantify2.api.dto.CommentDTO;
import epsi.mspr.plantify.plantify2.api.dto.CommentResponseDTO;
import epsi.mspr.plantify.plantify2.model.Plant;
import epsi.mspr.plantify.plantify2.model.Role;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.model.UserCommentPlant;
import epsi.mspr.plantify.plantify2.repository.PlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserCommentPlantRepository;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import epsi.mspr.plantify.plantify2.service.UserCommentPlantService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * UserCommentPlantImpl   is the class that implements the UserCommentPlantService interface
 */
@Service
@RequiredArgsConstructor
public class UserCommentPlantImpl implements UserCommentPlantService {
    private final UserRepository userRepository;
    private final UserCommentPlantRepository userCommentPlantRepository;
    private final PlantRepository plantRepository;
    @Override
    @Transactional
    public void saveComment(CommentDTO commentDTO) {
        //verify if the user exists
        User user = userRepository.findById(commentDTO.getBotanistId()).orElseThrow();
        //verify if the plant exists
        Plant plant = plantRepository.findById(commentDTO.getPlantId()).orElseThrow();
        //get the current date
        Instant date = Instant.now();
        //verify if the user is a botanist
        if (isBotanist(user)) {
            UserCommentPlant userCommentPlant = new UserCommentPlant();
            userCommentPlant.setComment(commentDTO.getComment());
            userCommentPlant.setBotanist(user);
            userCommentPlant.setPlant(plant);
            userCommentPlant.setDate(date);
            userCommentPlantRepository.save(userCommentPlant);
        }
    }

    @Override
    @Transactional
    public List<CommentResponseDTO> getComment(long plantId) {
        //get all the comments for a plant
        List<UserCommentPlant> userCommentPlants = userCommentPlantRepository.findByPlantId(plantId);
        List<CommentResponseDTO> commentResponseDTOS = new ArrayList<>();
        for(UserCommentPlant userCommentPlant : userCommentPlants){
            CommentResponseDTO commentResponseDTO = new CommentResponseDTO();
            commentResponseDTO.setComment(userCommentPlant.getComment());
            commentResponseDTO.setBotanist(userCommentPlant.getBotanist());
            commentResponseDTO.setDate(userCommentPlant.getDate());
            commentResponseDTOS.add(commentResponseDTO);
        }
        return commentResponseDTOS;
    }

    private boolean isBotanist(User user) {
        //verify if the user is a botanist
        for(Role role : user.getRoles()){
            if(role.getId() == 3){
                return true;
            }
        }
        return false;
    }
}
