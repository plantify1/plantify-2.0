package epsi.mspr.plantify.plantify2.service;

import epsi.mspr.plantify.plantify2.api.dto.PlantDTO;
import epsi.mspr.plantify.plantify2.api.dto.PlantResponseDTO;

import java.util.List;

public interface PlantService {
    void savePlant(PlantDTO plantDTO, Long ownerId);

    PlantResponseDTO getPlant(Long plantId);

    List<PlantResponseDTO> getAllPlants();
    void modifyPlant(PlantDTO plantDTO, Long plantId);
}
