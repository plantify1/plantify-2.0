package epsi.mspr.plantify.plantify2.service;

import epsi.mspr.plantify.plantify2.api.dto.UserDTO;
import epsi.mspr.plantify.plantify2.api.dto.UserWithRoleDTO;
import epsi.mspr.plantify.plantify2.model.User;

import java.util.List;

public interface UserService {
    boolean createUser(UserDTO user);
    List<UserDTO> getAllUsers();
    User getUser(Long userId);
    UserWithRoleDTO getUserWithRole(Long userId);
    void updateUser(Long userId, User user);
    User getUserByUidFirebase(String uidFirebase);
    User getUserByLogin(String login);
}
