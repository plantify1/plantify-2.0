package epsi.mspr.plantify.plantify2.service.impl;

import epsi.mspr.plantify.plantify2.api.dto.UserDTO;
import epsi.mspr.plantify.plantify2.api.dto.UserWithRoleDTO;
import epsi.mspr.plantify.plantify2.model.User;
import epsi.mspr.plantify.plantify2.repository.UserRepository;
import epsi.mspr.plantify.plantify2.service.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * UserServiceImpl   is the class that implements the UserService interface
 */

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Override
    @Transactional
    public boolean createUser(UserDTO userDTO) {
        //create a user with the DTO
        User user  = new User();
        user.setUidFirebase(userDTO.getUidFirebase());
        user.setLogin(userDTO.getLogin());
        user.setAddress(userDTO.getAddress());
        //verify if a user with the same login exists
        if (!userRepository.existsByLogin(user.getLogin())) {
            user = userRepository.save(user);
            userRepository.saveRole(user.getId(), 2L);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public List<UserDTO> getAllUsers() {
        //get all the users
        List<User> users = userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();
        for(User user : users){
            UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setLogin(user.getLogin());
            userDTO.setUidFirebase(user.getUidFirebase());
            userDTO.setAddress(user.getAddress());
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    @Override
    @Transactional
    public User getUser(Long userId) {
        //get the user
        return userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found !"));
    }

    @Override
    @Transactional
    public UserWithRoleDTO getUserWithRole(Long userId) {
        // get the user with his roles
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found !"));
        UserWithRoleDTO userWithRoleDTO = new UserWithRoleDTO();
        userWithRoleDTO.setId(user.getId());
        userWithRoleDTO.setLogin(user.getLogin());
        userWithRoleDTO.setUidFirebase(user.getUidFirebase());
        userWithRoleDTO.setAddress(user.getAddress());
        userWithRoleDTO.setRoles(user.getRoles());
        return userWithRoleDTO;
    }

    @Override
    @Transactional
    public void updateUser(Long userId, User user) {
        //update the user
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            User userToUpdate = userOptional.get();
            userToUpdate.setLogin(user.getLogin());
            userToUpdate.setUidFirebase(user.getUidFirebase());
            userToUpdate.setRoles(user.getRoles());
            userRepository.save(userToUpdate);
        }
    }

    @Override
    @Transactional
    public User getUserByUidFirebase(String uidFirebase) {
        return userRepository.getUserByUidFirebase(uidFirebase);
    }

    @Override
    @Transactional
    public User getUserByLogin(String login) {
        return userRepository.getUserByLogin(login);
    }
}
