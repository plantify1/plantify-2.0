package epsi.mspr.plantify.plantify2.service;

import epsi.mspr.plantify.plantify2.api.dto.MessageDTO;
import epsi.mspr.plantify.plantify2.api.dto.MessageResponseDTO;
import epsi.mspr.plantify.plantify2.model.User;

import java.util.List;

public interface MessageService {
    void saveMessage(MessageDTO messageDTO);
    List<MessageResponseDTO> getMessagesSend(String uidFirebase);
    List<MessageResponseDTO> getMessagesReceived(String uidFirebase);


}
