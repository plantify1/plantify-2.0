package epsi.mspr.plantify.plantify2.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

/**
 * Message   is the class that represents the message
 */
@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Message {

    @Id
    @SequenceGenerator(
            name = "message_id_sequence",
            sequenceName = "message_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "message_id_sequence"
    )
    private Long id;

    @Column(name = "message_body")
    private String messageBody;

    @Column(name = "message_date")
    private Instant messageDate;

    @Column(name = "message_object")
    private String messageObject;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "receiver_id")
    private User receiver;

}
