package epsi.mspr.plantify.plantify2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Plant   is the class that represents the plant entity
 */
@Entity
@Getter
@Setter
@ToString
@SuppressWarnings("JpaDataSourceORMInspection")
@RequiredArgsConstructor
public class Plant {

    @Id
    @SequenceGenerator(
            name = "plant_id_sequence",
            sequenceName = "plant_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "plant_id_sequence"
    )
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "to_guard")
    private boolean toGuard;

    @OneToMany(mappedBy="plant")
    @ToString.Exclude
    private List<Picture> pictures;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "user_id")
    private User owner;

    @OneToMany(mappedBy="plant")
    @ToString.Exclude
    @JsonIgnore
    private List<UserCommentPlant> comments;

    @OneToMany(mappedBy="plant")
    @ToString.Exclude
    @JsonIgnore
    private List<UserGuardPlant> guardians;

}
