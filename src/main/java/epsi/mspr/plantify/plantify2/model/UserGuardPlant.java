package epsi.mspr.plantify.plantify2.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

/**
 * UserGuardPlant   is the class that represents the user guard plant entity
 */

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class UserGuardPlant {
    @Id
    @SequenceGenerator(
            name = "userGuardPlant_id_sequence",
            sequenceName = "userGuardPlant_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "userGuardPlant_id_sequence"
    )
    private long id;

    @Column(name = "date", nullable = false)
    private Instant date;

    @ManyToOne
    @JoinColumn(name = "guard_id")
    private User guard;

    @ManyToOne
    @JoinColumn(name = "plant_id")
    private Plant plant;

}
