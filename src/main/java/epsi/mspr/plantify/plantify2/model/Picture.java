package epsi.mspr.plantify.plantify2.model;

import jakarta.persistence.*;
import lombok.*;
/*
 * Picture   is the class that represents the picture entity
 */

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Picture {
    @Id
    @SequenceGenerator(
            name = "picture_id_sequence",
            sequenceName = "picture_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "picture_id_sequence"
    )
    private long id;

    @Column(name = "picture", nullable = false)
    private String picture;

    @ManyToOne
    @JoinColumn(name = "plant_id")
    private Plant plant;

}
