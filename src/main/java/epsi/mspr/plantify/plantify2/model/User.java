package epsi.mspr.plantify.plantify2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * User   is the class that represents the user entity
 */
@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class User {
    @Id
    @SequenceGenerator(
            name = "user_id_sequence",
            sequenceName = "user_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_id_sequence"
    )
    private long id;

    @Column(name = "uid_firebase", nullable = false)
    private String uidFirebase;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "address")
    private String address;

    @JsonIgnore
    @OneToMany(mappedBy = "receiver")
    @ToString.Exclude
    private List<Message> messagesReceived;

    @JsonIgnore
    @OneToMany(mappedBy = "sender")
    @ToString.Exclude
    private List<Message> messagesSent;

    @JsonIgnore
    @ManyToMany(mappedBy = "users")
    @ToString.Exclude
    private List<Role> roles;

    @JsonIgnore
    @OneToMany(mappedBy="owner")
    @ToString.Exclude
    private List<Plant> plants;

    @JsonIgnore
    @OneToMany(mappedBy="botanist")
    @ToString.Exclude
    private List<UserCommentPlant> userCommentPlants;

    @JsonIgnore
    @OneToMany(mappedBy="guard")
    @ToString.Exclude
    private List<UserGuardPlant> plantsToGuard;

}
