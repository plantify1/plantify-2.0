package epsi.mspr.plantify.plantify2.model;

/**
 * UserRole   is the class that represents the different role in the application
 */
public enum UserRole {
    ROLE_USER, ROLE_ADMIN, ROLE_BOTANIST
}
