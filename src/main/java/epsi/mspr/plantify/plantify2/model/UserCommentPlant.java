package epsi.mspr.plantify.plantify2.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;
/**
 * UserCommentPlant   is the class that represents the user comment plant entity
 */

@SuppressWarnings("JpaDataSourceORMInspection")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class UserCommentPlant {
    @Id
    @SequenceGenerator(
            name = "userCommentPlant_id_sequence",
            sequenceName = "userCommentPlant_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "userCommentPlant_id_sequence"
    )
    private long id;

    @Column(name = "comment", nullable = false)
    private String comment;

    @Column(name = "date", nullable = false)
    private Instant date;

    @ManyToOne
    @JoinColumn(name = "botanist_id")
    private User botanist;

    @ManyToOne
    @JoinColumn(name = "plant_id")
    private Plant plant;

}
